Pod::Spec.new do |s|
    s.name                = "DataShaExtension"
    s.version             = "1.0.2"
    s.description         = "Extension for Data swift type for SHA digests"
    s.summary             = "Data sha extension"
    s.homepage            = "https://bitbucket.org/m_konovalov/datashaextension/overview"
    s.license             = { :type => "MIT", :file => "LICENSE" }
    s.author              = { "Mikhail Konovalov" => "m.konovalov@redmadrobot.com" }
    s.platform            = :ios, "9.0"
    s.source              = { :git => "https://m_konovalov@bitbucket.org/m_konovalov/datashaextension.git", :tag => s.version, :branch => "master" }
    s.source_files        = "Source/DataShaExtension/**/*.{h,m,swift}"
    s.requires_arc        = true
  
    s.preserve_paths      = 'Source/DataShaExtension/**/*'
    s.pod_target_xcconfig = {
      'SWIFT_INCLUDE_PATHS' => '$(PODS_ROOT)/DataShaExtension/Source/DataShaExtension/CCWrapperModulemap'
    }
  end
  