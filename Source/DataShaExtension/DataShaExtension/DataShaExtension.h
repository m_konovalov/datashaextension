//
//  DataShaExtension.h
//  DataShaExtension
//
//  Created by Mikhail Konovalov on 08/09/2017.
//  Copyright © 2017 Redmadrobot OOO. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for DataShaExtension.
FOUNDATION_EXPORT double DataShaExtensionVersionNumber;

//! Project version string for DataShaExtension.
FOUNDATION_EXPORT const unsigned char DataShaExtensionVersionString[];
